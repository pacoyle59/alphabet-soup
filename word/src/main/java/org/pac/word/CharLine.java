package org.pac.word;

/**
 * @author Peter Coyle
 *  A class that contains:
 *   a string created from the char matrix 
 *   a corresponding list with the (Row,Col) location of each character 
 *   the string reversed
 *   the reversed list of char locations
 *   
 */
public class CharLine {
	private static final String NOT_FOUND ="Not found";
	
	
  int  len;
  String line;
  String reverseline;
  MatLoc[] locations;
  MatLoc[] revlocations;
  
  /**
 * @param target - String to look for
 * @return A string containing the passed in target and starting and ending locations
 *         OR
 *         "Not found" if that string was not found
 */
public String searchLine(String target)
 {
	  int startPos=0;
	  int endPos=0;
	  String retStr;
	  if ( this.line.contains(target)) {
		 startPos= this.line.indexOf(target);
		 endPos= startPos+(target.length()-1);
		 retStr = target + " "+this.locations[startPos].row+":"+this.locations[startPos].col+" "+this.locations[endPos].row+":"+this.locations[endPos].col;
		 return retStr;
	  }else if (this.reverseline.contains(target)) {
		 startPos= this.reverseline.indexOf(target);
		 endPos= startPos+(target.length()-1);
		 retStr = target + " "+this.revlocations[startPos].row+":"+this.revlocations[startPos].col+" "+this.revlocations[endPos].row+":"+this.revlocations[endPos].col;
		 return retStr;
	  }else {
		  return NOT_FOUND;	
	  }
		  
 }
  public void setLen(int len) {
  this.len = len;
 }
  public String getLine() {
	  return this.line;
  }
  public String getReverseline() {
	  return this.reverseline;
  }
  public void setrevlocations(int size) {
	  int j=0;
//	  System.out.println("Revloc size: "+size);
	  this.revlocations = new MatLoc[size];
	  for (int i= size-1; i>=0; i--) {
		  this.revlocations[j++]= new MatLoc(this.locations[i].row, this.locations[i].col);
	  }
  }
}
