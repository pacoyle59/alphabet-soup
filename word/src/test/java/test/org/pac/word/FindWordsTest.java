package test.org.pac.word;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.pac.word.CharLine;
import org.pac.word.FindWords;
class FindWordsTest {


	private static char[][] charMat1 = {
			{'a', 'b', 'c', 'd', 'e', 'f'}, 
            {'g', 'h', 'i', 'j', 'k', 'l'}, 
            {'m', 'n', 'o', 'p', 'q', 'r'},
            {'s', 't', 'u', 'v', 'w', 'x'},
            {'y', 'z', 'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h', 'i', 'j'}
            };
	
           
	private static char[][] charMat2by8 = {
			{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'},
			{'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r'}
            };
	private static char[][] charMat8by2 = {
			{'a', 'b'},
			{'c', 'd'},
			{'e', 'f'}, 
            {'g', 'h'},
            {'i', 'j'},
			{'k', 'l'}, 
            {'m', 'n'},
            {'o', 'p'}
            };
	@Test
	void testCreateRows() throws Exception {		
		List<CharLine> rows = null;
		rows=FindWords.createRows(charMat1,6,6);
		   assertEquals( rows.get(0).getLine(), "abcdef");		
	}

	@Test
	void testCreateRows2by8() throws Exception {		
		List<CharLine> rows = null;
		rows= FindWords.createRows(charMat2by8,2,8);
        assertEquals( rows.get(0).getLine(), "abcdefgh");		
	}
	
	@Test
	void testCreateRows8by2() throws Exception {
		List<CharLine> rows = null;
		rows= FindWords.createRows(charMat8by2,8,2);
        assertEquals( rows.get(0).getLine(), "ab");	
	}
	
	@Test
	void testCreateRowsRev2by8() throws Exception {		
		List<CharLine> rows = null;
		rows= FindWords.createRows(charMat2by8,2,8);
        assertEquals( rows.get(0).getReverseline(), "hgfedcba");
	}
	
	@Test
	void testCreateRowsRev8by2() throws Exception {		
		List<CharLine> rows = null;
		rows= FindWords.createRows(charMat8by2,8,2);
        assertEquals( rows.get(0).getReverseline(), "ba");	
	}
	
	
	@Test
	void testCreateCols() throws Exception {		
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat1,6,6);
        assertEquals( cols.get(0).getLine(), "agmsye");		
	}
	
	@Test
	void testCreateCols2by8() throws Exception {		
		List<CharLine> cols = null;
		cols= FindWords.createCols(charMat2by8,2,8);
        assertEquals( cols.get(0).getLine(), "ak");		
	}
	
	@Test
	void testCreateCols8by2() throws Exception {
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat8by2,8,2);
        assertEquals( cols.get(0).getLine(), "acegikmo");	
	}
	
	@Test
	void testCreateColsRev2by8() throws Exception {		
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat2by8,2,8);
        assertEquals( cols.get(0).getReverseline(), "ka");
	}
	
	@Test
	void testCreateColsRev8by2() throws Exception {		
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat8by2,8,2);
        assertEquals( cols.get(0).getReverseline(), "omkigeca");	
	}
	
	
	@Test
	void testCreateRightDiag() throws Exception {		
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat1,6,6);
		 assertEquals( rtDiags.get(0).getReverseline(), "fy");
		 assertEquals( rtDiags.get(0).getLine(), "yf");
	}
	
	@Test
	void testCreateRightDiags2by8() throws Exception {		
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat2by8,2,8);
        assertEquals( rtDiags.get(0).getLine(), "al");		
        assertEquals( rtDiags.get(6).getLine(), "gr");		
	}
	
	@Test
	void testCreateRightDiags8by2() throws Exception {
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat8by2,8,2);
        assertEquals( rtDiags.get(0).getLine(), "mp");	
        assertEquals( rtDiags.get(6).getLine(), "ad");	
	}
	
	
	@Test
	void testCreateRightDiagsRev2by8() throws Exception {		
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat2by8,2,8);
        assertEquals( rtDiags.get(3).getReverseline(), "od");
        assertEquals( rtDiags.get(5).getReverseline(), "qf");
	}
	
	@Test
	void testCreateRightDiagsRev8by2() throws Exception {		
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat8by2,8,2);
        assertEquals( rtDiags.get(1).getReverseline(), "nk");	
        assertEquals( rtDiags.get(4).getReverseline(), "he");	
	}
	

	@Test
	void testCreateLeftDiag() throws Exception {		
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat1,6,6);
		 assertEquals( lfDiags.get(0).getLine(), "di");
		 assertEquals( lfDiags.get(5).getReverseline(), "ytoje");
			
	}
	@Test
	void testCreateLeftDiags2by8() throws Exception {		
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat2by8,2,8);
        assertEquals( lfDiags.get(0).getLine(), "hq");	
        assertEquals( lfDiags.get(6).getLine(), "bk");	
	}
	
	@Test
	void testCreateLeftDiags8by2() throws Exception {
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat8by2,8,2);
        assertEquals( lfDiags.get(0).getLine(), "no");	
        assertEquals( lfDiags.get(6).getLine(), "bc");	
	}
	
	@Test
	void testCreateLeftDiagsRev2by8() throws Exception {		
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat2by8,2,8);
        assertEquals( lfDiags.get(3).getReverseline(), "ne");
        assertEquals( lfDiags.get(4).getReverseline(), "md");
	}
	
	@Test
	void testCreateLeftDiagsRev8by2() throws Exception {		
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat8by2,8,2);
        assertEquals( lfDiags.get(5).getReverseline(), "ed");	
        assertEquals( lfDiags.get(6).getReverseline(), "cb");	
	}

}
