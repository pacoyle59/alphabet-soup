package test.org.pac.word;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.pac.word.CharLine;
import org.pac.word.FindWords;

class CharLineTest {
	private static char[][] charMat1 = {
			{'a', 'b', 'c', 'd', 'e', 'f'}, 
            {'g', 'h', 'i', 'j', 'k', 'l'}, 
            {'m', 'n', 'o', 'p', 'q', 'r'},
            {'s', 't', 'u', 'v', 'w', 'x'},
            {'y', 'z', 'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h', 'i', 'j'}
            };

	@Test
	void testSearchLineRows() throws Exception {
		List<CharLine> rows = null;		
		rows=FindWords.createRows(charMat1,6,6);
		 assertEquals( rows.get(0).searchLine("abc"), "abc 0:0 0:2");	
	}
	
	@Test
	void testSearchLineRowsRev() throws Exception {
		List<CharLine> rows = null;		
		rows=FindWords.createRows(charMat1,6,6);
		 assertEquals( rows.get(0).searchLine("cba"), "cba 0:2 0:0");
	}
	
	@Test
	void testSearchLineRowsNotFound() throws Exception {
		List<CharLine> rows = null;		
		rows=FindWords.createRows(charMat1,6,6);
		 assertEquals( rows.get(0).searchLine("xxx"), "Not found");
	}
	
	@Test
	void testSearchLineCols() throws Exception {
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat1,6,6);
		 assertEquals( cols.get(0).searchLine("agm"), "agm 0:0 2:0");	
	}
	@Test
	void testSearchLineColssRev() throws Exception {
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat1,6,6);
		 assertEquals( cols.get(0).searchLine("mga"), "mga 2:0 0:0");
	}
	
	@Test
	void testSearchLineColsNotFound() throws Exception {
		List<CharLine> cols = null;	
		cols= FindWords.createCols(charMat1,6,6);
		 assertEquals( cols.get(0).searchLine("xxx"), "Not found");
	}
	
	@Test
	void testSearchLineRtDiags() throws Exception {
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat1,6,6);
		 assertEquals( rtDiags.get(1).searchLine("szg"), "szg 3:0 5:2");
	}
	
	@Test
	void testSearchLineRtDiagsRev() throws Exception {
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat1,6,6);
		 assertEquals( rtDiags.get(1).searchLine("gzs"), "gzs 5:2 3:0");
	}
	
	@Test
	void testSearchLineRtDiagsNotFound() throws Exception {
		List<CharLine> rtDiags = null;
		rtDiags = FindWords.createRightDiag(charMat1,6,6);
		 assertEquals( rtDiags.get(0).searchLine("xxx"), "Not found");
	}
	
	@Test
	void testSearchLineLtDiags() throws Exception {
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat1,6,6);
		 assertEquals( lfDiags.get(2).searchLine("gbw"), "gbw 5:2 3:4");
	}
	
	@Test
	void testSearchLineLtDiagsRev() throws Exception {
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat1,6,6);
		 assertEquals( lfDiags.get(2).searchLine("wbg"), "wbg 3:4 5:2");
	}
	
	@Test
	void testSearchLineLtDiagsNotFound() throws Exception {
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat1,6,6);
		 assertEquals( lfDiags.get(0).searchLine("xxx"), "Not found");

	}
}
